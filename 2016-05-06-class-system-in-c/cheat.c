#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define NEW(klass) { malloc(sizeof(klass)); }

typedef struct User_struct {
	char *firstName, *lastName;
	void (*sayHello)(struct User_struct *);
} User;

User *user_init(User *this);
void user_sayHello(User *this);

User *User_init(User *this) {
	this->sayHello = &user_sayHello;
	return this;
}

void User_sayHello(User *this) {
	printf("Hello, %s %s\n", this->firstName, this->lastName);
}

int main() {
	User *user = NEW(User);
	user_init(user);
	user->firstName = "Abby";
	user->lastName = "Normal";
	user->sayHello(user);
	free(user);

	return 0;
}
