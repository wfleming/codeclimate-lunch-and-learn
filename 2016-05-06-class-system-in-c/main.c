#include <stdlib.h>
#include <string.h>
#include <stdio.h>

typedef void*(*t_initFn)(void*);
void *alloc(size_t size, t_initFn);

#define NEW(klass) alloc(sizeof(klass), (t_initFn)&klass##_init);

void *alloc(size_t size, t_initFn initFn) {
	void *i = malloc(size);
	return initFn(i);
}

typedef struct User_struct {
	char *firstName, *lastName;
	void (*sayHello)(struct User_struct *);
} User;

User *User_init(User *this);
void User_sayHello(User *this);

User *User_init(User *this) {
	this->sayHello = &User_sayHello;
	return this;
}

void User_sayHello(User *this) {
	printf("Hello, %s %s\n", this->firstName, this->lastName);
}

int main() {
	User *user = NEW(User);
	user->firstName = "Abby";
	user->lastName = "Normal";
	user->sayHello(user);
	free(user);

	return 0;
}
