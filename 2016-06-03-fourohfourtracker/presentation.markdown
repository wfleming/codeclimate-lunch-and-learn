# Building With Tools You Don't Understand

(… for fun and profit.)

<small>(… okay, no actual profit.)</small>

---

<img src="tweet.png" />

---

<img src="incredibles.gif" />

---

Wait, I'm a programmer.

<img src="kermit.gif" />

---

<table>
  <tr>
    <td style="width:50%;">
      <img src="elm.png" style="min-width: 10rem; max-width: 10rem; min-height: 10rem;" />
    </td>
    <td style="width:50%;">
      <img src="elixir.png" style="min-width: 10rem; max-width: 10rem; min-height: 10rem;" />
    </td>
  </tr>
  <tr>
    <td>Elm</td>
    <td>Elixir</td>
  </tr>
  <tr style="font-weight: lighter;">
    <td><small>&ldquo;The best of functional programming in your browser.&rdquo;</small></td>
    <td><small>&ldquo;Elixir is a dynamic, functional language designed for building scalable and maintainable applications.&rdquo;</small></td>
  </tr>
</table>

---

## First Impressions

---

### Elm

<table>
  <tr>
    <th>Pro</th>
    <th>Con</th>
  </tr>
  <tr style="font-weight: lighter;">
    <td><ul style="vertical-align: top;">
      <li>It's like Haskell!</li>
      <li>Batteries included</li> <!-- web-app focused, makes doing that easy -->
    </ul></td>
    <td><ul style="vertical-align: top;">
      <li>It's not <i>quite</i> like Haskell</li> <!-- syntax that is similar, different method names: context switch cost -->
      <li>Immature</li> <!-- Lots of low-hanging fruit & syntax sugar that isn't there yet -->
      <li>No type classes</li> <!-- Falls under immature, but worth calling out -->
    </ul></td>
  </tr>
</table>

---

### Elixir

<table>
  <tr>
    <th>Pro</th>
    <th>Con</th>
  </tr>
  <tr style="font-weight: lighter;">
    <td><ul style="vertical-align: top;">
      <li>Fast (very fast!)</li>  <!-- ~75x faster than Ruby, ~10x throughput with lower CPU. It's not C, but for a "scripting" language it's very performant -->
      <li>Native concurrency</li> <!-- Running *thousands* of processes is NBD -->
      <li>Ruby-esque syntax</li> <!-- familiar, easy to pick up -->
      <li>FP-focused</li> <!-- common paradigm. Features like pattern matching. -->
    </ul></td>
    <td><ul style="vertical-align: top;">
      <li>Ruby-esque type system</li>
    </ul></td>
  </tr>
</table>

<small>(Aside: Phoenix is eating Rails' lunch.)</small>

---

## Demo

---

<img src="cc-results.png" />

<img src="laughing.gif" style="min-width: 30%; min-height:30%" />

---

[https://github.com/wfleming/fourohfourfinder](https://github.com/wfleming/fourohfourfinder)

[https://fourohfourtracker.herokuapp.com/](https://fourohfourtracker.herokuapp.com/)

<small style="margin-top: 10rem; font-size: 40%;">fin.</small>

---
