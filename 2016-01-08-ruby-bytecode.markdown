# Ruby Bytecode

---

# In The Beginning…

* Pre-1.9, as near as I can tell, Ruby didn't *have* bytecode
* YARV made bytecode standard in 1.9

---

## Ruby

```ruby
# a + b
push_local                 0    # a
push_local                 1    # b
meta_send_op_plus          :+
ret
```

## x86

```x86
POP EAX      ; assume operands are on stack
POP EBX
ADD EAX, EBX ; add registers, result in EAX
```

<!--
 Takeaway: Ruby bytecode looks similar, but is in fact higher level
-->

---

# Ruby 2.3

## What's new?

Ruby 2.3 has added the ability to load iseqs (bytecode) directly, rather than loading source & then parsing it into iseq.

## So what?

---

# Bytecode caching
## `yomikomu`

https://github.com/ko1/yomikomu

Yomikomu is a gem adding bytecode caching to Ruby 2.3.
Similar to `.pyc` files in Python, it writes compiled bytecode of Ruby sources to `.yarb` files.
When you do a `require`, the `.yarb` file will be loaded directly if it exists & is younger than the source file.

<!--
https://news.encinc.com/2016/01/07/Bytecode-cache-is-experimentally-released-in-Ruby2.3-53073
Advantage here is you can skip the compilation stage when source is unchanged, giving you faster app startup.
If bytecode caching becomes common/standard, then that opens up performance improvement opportunities because the compile phase can spend more time on optimization.
-->

---

# Ruby 3x3

One of the stated goals of Ruby 3.0 development is to make it 3 times as fast as 2.0.

---

